<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>     

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<table border="2" width="70%" cellpadding="2"> 
		<tr>
			<th>ID</th>
			<th>State</th>
		</tr>
			<c:forEach var="act" items="${stateList}">
				<tr>
					<td>${act.id}</td>
					<td>${act.stateName}</td>
				</tr>
			</c:forEach>
</table>

</body>
</html>
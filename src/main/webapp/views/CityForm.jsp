<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@ page isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>     

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>CityForm</title>
</head>
<body>
	<form action="/MVC_Aplication/cityFormController" method="post">
		Id: <input type="text" name="id">
		New City: <input type="text" name="city">
  		<select name="state">
    		<c:forEach var="act" items="${stateList}">
				<tr>
					<option value="${act.stateName}">"${act.stateName}"</option>
				</tr>
			</c:forEach>
  		</select>
  		<br><br>
  		<input type="submit"  value="Save City">
	</form>
	
</body>
</html>
package com.softtek.academia.javaweb.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academia.javaweb.daos.CityFormDao;
import com.softtek.academia.javaweb.daos.StateFormDao;

/**
 * Servlet implementation class CityFormController
 */
public class CityFormController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CityFormController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Para checar error StateDao");
		String variable = request.getParameter("variable");
		System.out.println(variable);
		
		if(variable.equals("1")) {
			System.out.println("Entra x1");
			CityFormDao cityDao = new CityFormDao();
			System.out.println(cityDao.cityList().toString());
			request.setAttribute("cityList",cityDao.cityList());
			request.getRequestDispatcher("/views/CityList.jsp").forward(request, response);
		}else {
			System.out.println("Entra x0");
		StateFormDao stateDao = new StateFormDao();
		request.setAttribute("stateList",stateDao.stateList());
		request.getRequestDispatcher("/views/CityForm.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Entro al DoPost");
		CityFormDao cityDao = new CityFormDao();
		
		//Params...
		String id = request.getParameter("id");
		String city = request.getParameter("city");
		String state = request.getParameter("state");
		
		System.out.println(id);
		System.out.println(city);
		System.out.println(state);
		
		if(!id.equals("") && !state.equals("") && !city.equals("")) {
			cityDao.saveCity(id,city,state);
		}
		
		System.out.println(cityDao.cityList().toString());
		request.setAttribute("cityList",cityDao.cityList());
		request.getRequestDispatcher("/views/CityList.jsp").forward(request, response);
	/*	if(!id.equals("") && !state.equals("")) {
			stateDao.saveState(id, state);
		}
		
		request.setAttribute("stateList",stateDao.stateList());
		System.out.println(stateDao.stateList().toString());
		System.out.println(stateDao.stateList().get(0).getId());
		System.out.println(stateDao.stateList().get(0).getStateName());
		request.getRequestDispatcher("/views/StateList.jsp").forward(request, response);
		*/
		//for(int  i = 0 ; i < stateDao.stateList().size(); i++) {
			//System.out.println(stateDao.stateList().toString());
		//}
		//stateDao.stateList();
		//System.out.println(stateDao.stateList().toString());
		//doGet(request, response);
	}

}

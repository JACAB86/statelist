package com.softtek.academia.javaweb.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academia.javaweb.daos.StateFormDao;

/**
 * Servlet implementation class StateFormController
 */
public class StateFormController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StateFormController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StateFormDao stateDao = new StateFormDao();
		request.setAttribute("stateList",stateDao.stateList());
		System.out.println(stateDao.stateList().toString());
		request.getRequestDispatcher("/views/StateList.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		StateFormDao stateDao = new StateFormDao();
		
		//Params...
		String id = request.getParameter("id");
		String state = request.getParameter("state");
		
		if(!id.equals("") && !state.equals("")) {
			stateDao.saveState(id, state);
		}
		
		request.setAttribute("stateList",stateDao.stateList());
		System.out.println(stateDao.stateList().toString());
		System.out.println(stateDao.stateList().get(0).getId());
		System.out.println(stateDao.stateList().get(0).getStateName());
		request.getRequestDispatcher("/views/StateList.jsp").forward(request, response);
		
		//for(int  i = 0 ; i < stateDao.stateList().size(); i++) {
			//System.out.println(stateDao.stateList().toString());
		//}
		//stateDao.stateList();
		//System.out.println(stateDao.stateList().toString());
		//doGet(request, response);
	}

}

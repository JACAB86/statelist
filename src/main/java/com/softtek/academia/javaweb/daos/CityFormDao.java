package com.softtek.academia.javaweb.daos;

import java.util.ArrayList;

import com.softtek.academia.javaweb.beans.CityModel;
import com.softtek.academia.javaweb.services.MVCService;

public class CityFormDao {
		MVCService service = new MVCService();
		ArrayList<CityModel> cityList = new ArrayList<CityModel>();
	
		public ArrayList<CityModel> cityList(){
			
			//this.service = new MVCService();
			
			this.cityList = service.getAllCities();
			return cityList;
		}
		
		
	public ArrayList<CityModel> saveCity(String id,String city,  String state){
		//Validation Data must be here(Front & Back)
		Integer i = Integer.parseInt(id);
		CityModel newBean = new CityModel();
		newBean.setId(i);
		newBean.setCity(city);
		newBean.setState(state);
		
		
		
		service.saveCity(newBean);
		return cityList();
	}
	
}

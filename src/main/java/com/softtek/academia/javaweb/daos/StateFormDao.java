package com.softtek.academia.javaweb.daos;

import java.util.ArrayList;

import com.softtek.academia.javaweb.beans.StateModel;
import com.softtek.academia.javaweb.services.MVCService;

public class StateFormDao {
	MVCService service = new MVCService();
	ArrayList<StateModel> stateList = new ArrayList<StateModel>();
	
	public ArrayList<StateModel> stateList(){
		
		//this.service = new MVCService();
		
		this.stateList = service.getAllStates();
		return stateList;
		
		//Here needs to be some bussiness validations
		// the information that the service can provide
		/*
		if(this.stateList != null) {
			return this.stateList;
		} else {
			return null;
		}*/
	}
	
	public ArrayList<StateModel> saveState(String id, String state){
		//Validation Data must be here(Front & Back)
		Integer i = Integer.parseInt(id);
		StateModel newBean = new StateModel();
		newBean.setId(i);
		newBean.setStateName(state);
		
		
		
		service.saveState(newBean);
		return stateList();
	}
}

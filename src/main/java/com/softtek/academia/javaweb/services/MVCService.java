package com.softtek.academia.javaweb.services;

import java.util.ArrayList;

import com.softtek.academia.javaweb.beans.CityModel;
import com.softtek.academia.javaweb.beans.StateModel;

public class MVCService {
	

	private static ArrayList<StateModel> stateList;
	private static ArrayList<CityModel> cityList;
	
	static {
		stateList = new ArrayList<StateModel>();
		cityList = new ArrayList<CityModel>();

		
		int id = 0;
		String name = "Estado";
		
		for(int i = 1 ; i <=5 ; i++ ) {
			StateModel myBean = new StateModel();
			myBean.setId(i + id);
			myBean.setStateName(name+" "+ i );
			stateList.add(myBean);	
		}
		
		for(int i = 1 ; i <=3 ; i++ ) {
			CityModel myBean = new CityModel();
			myBean.setId(i);
			myBean.setCity("City "+ i );
			myBean.setState("State "+ i );
			cityList.add(myBean);	
		}
		
	}
	
	public ArrayList<StateModel> getAllStates(){
		return stateList = getMyStateList();
		
		//Get DB connection
	}
	
	public ArrayList<CityModel> getAllCities(){
		return cityList = getMyCityList();
		
		//Get DB connection
	}


	public ArrayList<StateModel> saveState(StateModel newBean) {
		System.out.println("ENTRA");
		stateList.add(newBean);
		return stateList;
	}
	
	public ArrayList<CityModel> saveCity(CityModel newBean) {
		System.out.println("ENTRA");
		cityList.add(newBean);
		return cityList;
	}
	
	
	//This service method  will simulate the query to Data Base
	private ArrayList<StateModel> getMyStateList() {
		
		//Get DB connection
		if(stateList.equals(null)) {
			//ArrayList arr = new ArrayList<StateModel>();
			StateModel myBean = new StateModel();
			String name = "Estado";
			for(int i = 1 ; i <=5 ; i++ ) {
			myBean.setId(i);
			myBean.setStateName(name + i);
			stateList.add(myBean);
			
		}
		
		
	}
	return stateList;
	
	}
	
	private ArrayList<CityModel> getMyCityList() {
		
		//Get DB connection
		if(cityList.equals(null)) {
			//ArrayList arr = new ArrayList<StateModel>();
			CityModel myBean = new CityModel();
			String name = "Estado";
			String city = "Ciudad";
			for(int i = 1 ; i <=5 ; i++ ) {
			myBean.setId(i);
			myBean.setState(name+" "+ i);
			myBean.setCity(city+" "+ i);
			cityList.add(myBean);
			
		}
		
		
	}
	return cityList;
	
	}
}
